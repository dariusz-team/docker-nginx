### Usage

    docker run -d -p 80:80 advice/nginx

#### Attach persistent/shared directories

    docker run -d -p 80:80 -v <sites-enabled-dir>:/etc/nginx/sites-enabled -v <certs-dir>:/etc/nginx/certs <html-dir>:/app advice/nginx

After few seconds, open `http://<host>` to see the welcome page.
